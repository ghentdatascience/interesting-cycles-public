clear;
clc;

fileID = fopen('TradeGraphcsv_id.txt', 'r');
Aorig = fscanf(fileID, '%i %i %f\n', [3 Inf]);
fclose(fileID);

numberofnodes = max(max([Aorig(1,:); Aorig(2,:)]));
values = Aorig(3,:); % Rounding the trade data to natural numbers -> geometric distributio
values = values./10;


A = sparse(Aorig(1,:), Aorig(2,:), values, numberofnodes, numberofnodes);

ind = 2*ones(1,numberofnodes); % All the countries not in a prior belief
ind([61 211]) = 1; % Trade Agreement 1

[lar, lac, lalpha, ldiag, jr, jc, ev, cum] = Newton_Geometric_Blocks(A, 150, ind);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Writing the weights to a file.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

drows = full(sum(A,2));
dcols = full(sum(A,1))';

bins = cell(1, max(ind)); % Creating empty cells. 
nu_r = cell(1, max(ind));
nu_c = cell(1, max(ind));
j_r = cell(1, max(ind));
j_c = cell(1, max(ind));

l_r = zeros(1, max(ind));
l_c = zeros(1, max(ind));
occur_r = cell(1, max(ind));
occur_c = cell(1, max(ind));

for i=1:max(ind)
    bins{i} = find(ind==i); % Every cell element is a vector with the resp. bin nodes.
    [nu_r{i}, ~, j_r{i}] = unique(drows(bins{i})); % Finding the unique row degrees in the ith bin.
    [nu_c{i}, ~, j_c{i}] = unique(dcols(bins{i})); % Finding the unique column degrees in the ith bin. 
    l_r(i) = length(nu_r{i});   
    l_c(i) = length(nu_c{i}); 
end

csumlr = cumsum(l_r);
csumlc = cumsum(l_c);

LAR = zeros(numberofnodes,1);
LAC = zeros(numberofnodes,1);

temp1 = lar(1:csumlr(1));
temp2 = lac(1:csumlc(1));
LAR(bins{1}) = temp1(j_r{1});
LAC(bins{1}) = temp2(j_c{1});
for i=2:length(bins)
    temp1 = lar(csumlr(i-1)+1:csumlr(i));
    temp2 = lac(csumlc(i-1)+1:csumlc(i));
    LAR(bins{i}) = temp1(j_r{i});
    LAC(bins{i}) = temp2(j_c{i});
end

la_from = LAR(Aorig(1,:));
la_to = LAC(Aorig(2,:));

la_block = zeros(length(Aorig(1,:)),1);
pos = find(ind(Aorig(1,:)) == ind(Aorig(2,:)) & ind(Aorig(1,:)) ~= 2); % Finding the edges in the blocks.
la_block(pos) = lalpha(ind(Aorig(1,pos)));


% This gives the exact weight Prob(X=x) in the geometric distribution
%weights = exp(la_from+la_to).^(values');
%weights = (1-exp(la_from+la_to)).*weights;

% This gives the cumulative weights Prob(X>=x) of the geometric
% distribution, i.e. Prob(X>=x) = (1-p)^x

p = 1-exp(la_from+la_to+la_block);
weights = (1-p).^(values');

from = Aorig(1,:);
to = Aorig(2,:);
fileID = fopen('TradeData_Geometric_probabilities_Agreements.txt', 'w');
fprintf(fileID,'%i %i %2.5e\r\n', [from; to; weights']); 
fclose(fileID);