function [G, H] = GH_Geometric_Blocks(x, indicesDiag, l_r, l_c, occur_r, occur_c, d_r, d_c, ActualBinSums, ActualDiagonalSum)
% G and H of the Reduced Lagrange (Geometric) dual function w block prior
% occur_r, occur_c, d_r and d_c are cells.
% x = 1x(sumlr+sumlc+numberofbins-1)-vector

% GRADIENT
numberofbins = length(occur_r);
csumlr = cumsum(l_r);
sumlr = csumlr(numberofbins);
csumlc = cumsum(l_c);
sumlc = csumlc(numberofbins);

Ar = repmat(x(1:sumlr)', 1, sumlc);
Ac = repmat(x(sumlr+1:sumlr+sumlc), sumlr, 1);

MatrixAlpha = zeros(sumlr, sumlc);
startr = 1;
startc = 1;
for i=1:numberofbins-1
    MatrixAlpha(startr:csumlr(i), startc:csumlc(i)) = ones(csumlr(i)-startr+1, csumlc(i)-startc+1)*x(sumlr+sumlc+i);
    startr = csumlr(i)+1;
    startc = csumlc(i)+1;
end

MatrixDiag = zeros(sumlr, sumlc);
MatrixDiag(indicesDiag) = x(sumlr+sumlc+numberofbins-1+1);
% indicesDiag = lin. indices of the diagonal elements in the red. matrix

MatrixOccurR = repmat(occur_r{1}, 1, sumlc); % Defining the Occurence matrices.
MatrixOccurC = repmat(occur_c{1}', sumlr, 1);
for i=2:numberofbins
    MatrixOccurR = [MatrixOccurR; repmat(occur_r{i}, 1, sumlc)];
    MatrixOccurC = [MatrixOccurC repmat(occur_c{i}', sumlr, 1)];
end

MatrixOccurD = zeros(sumlr, sumlc);
val = unique(indicesDiag);
MatrixOccurD(val) = histc(indicesDiag,val);

B1 = exp(Ar+Ac+MatrixAlpha);
B1 = B1./(1-B1); % GEOMETRIC DISTRIBUTION
B1 = (MatrixOccurR.*MatrixOccurC-MatrixOccurD).*B1;
B2 = exp(Ar+Ac+MatrixAlpha+MatrixDiag);
B2 = B2./(1-B2);
B2 = MatrixOccurD.*B2;
B = B1 + B2;

DR = occur_r{1}.*d_r{1};
DC = occur_c{1}.*d_c{1};
for i=2:numberofbins
    DR = [DR; occur_r{i}.*d_r{i}];
    DC = [DC; occur_c{i}.*d_c{i}];
end

CurrentBinSums = zeros(1,max(numberofbins)-1); % The last bin is not counted in the prior.
startr = 1;
startc = 1;
for i=1:max(numberofbins)-1
    CurrentBinSums(i) =  sum(sum(B(startr:csumlr(i), startc:csumlc(i))));
    startr = csumlr(i)+1;
    startc = csumlc(i)+1;
end

CurrentDiagSum = sum(B2(val));

G1 = sum(B,2)-DR;
G2 = sum(B)'-DC;
G3 = (CurrentBinSums-ActualBinSums)';
G4 = CurrentDiagSum - ActualDiagonalSum;
G = [G1; G2; G3; G4];


% HESSIAN.
Z = B1./(1-exp(Ar+Ac+MatrixAlpha)) +B2./(1-exp(Ar+Ac+MatrixAlpha+MatrixDiag));

D1 = diag(sum(Z, 2));
D2 = diag(sum(Z));

CurrentBinSumsZ = zeros(1,max(numberofbins)-1);
startr = 1;
startc = 1;
for i=1:max(numberofbins)-1
    CurrentBinSumsZ(i) =  sum(sum(Z(startr:csumlr(i), startc:csumlc(i))));
    startr = csumlr(i)+1;
    startc = csumlc(i)+1;
end
D3 = diag(CurrentBinSumsZ);


binsr=cell(1,numberofbins-1);
binsc=cell(1,numberofbins-1);
startr=1;
startc=1;
for i=1:numberofbins-1 
    binsr{i} = startr:csumlr(i);
    binsc{i} = startc:csumlc(i);
    startr=csumlr(i)+1;
    startc=csumlc(i)+1;
end

DRA = zeros(sumlr, numberofbins-1);
DCA = zeros(sumlc, numberofbins-1);

for i=1:numberofbins-1
    DRA(binsr{i},i) = sum(Z(binsr{i}, binsc{i}),2);
    DCA(binsc{i},i) = sum(Z(binsr{i}, binsc{i}),1);
end

% Approximating the Hessian (the dependence between the diag. and other
% parameters is not included)
sumDiag = sum(sum(B2./(1-exp(Ar+Ac+MatrixAlpha+MatrixDiag))));
H = [D1 Z DRA zeros(sumlr,1); Z' D2 DCA zeros(sumlc,1); DRA' DCA' D3 zeros(numberofbins-1,1); zeros(1,sumlr+sumlc+numberofbins-1) sumDiag];