from CycleAlgos import *

with open('p2p-Gnutella24.txt', 'r') as file:
    # Creating the graph, and storing the explicit trade volumes as weights
    G = nx.DiGraph()
    for line in file:
        vals = line.split()
        G.add_edge(int(vals[0]),int(vals[1]))

d_weights = {}
for e in G.edges():
    d_weights[e] = 1
    
ScalingExperiment(G, 0.02, 1, d_weights, 200, 'Scaling_Gnutella_noCycle_Q1.txt', 'Scaling_Gnutella_yesCycle_Q1.txt')
ScalingExperiment(G, 0.02, 3, d_weights, 200, 'Scaling_Gnutella_noCycle_Q3.txt', 'Scaling_Gnutella_yesCycle_Q3.txt')
ScalingExperiment(G, 0.02, 5, d_weights, 200, 'Scaling_Gnutella_noCycle_Q5.txt', 'Scaling_Gnutella_yesCycle_Q5.txt')
