import glob
import csv
import networkx as nx
import numpy as np
from math import log
from CycleAlgos import *


path = 'C:\\Users\\fawadria\\Desktop\\Interesting Cycles\\TradeData'

files = glob.glob(path + "/*.csv")

'''
def writeTradeDAdj():
    teller = 0
    with open('TradeGraphcsv.txt', 'w') as adjListTrade:
        for f in files:
            with open(f, 'r') as currentFile:
                next(currentFile) # Skipping the header
                teller = 1
                reader = csv.reader(currentFile, skipinitialspace=True)
                for line in reader:
                    if teller<6:
                        adjListTrade.write(line[0]+'\t'+line[2]+'\t'+line[6]+'\n') # write export
                    else:
                        adjListTrade.write(line[2]+'\t'+line[0]+'\t'+line[6]+'\n') # write import
                    teller+=1
                    if teller>10:
                        break
'''

countries = []
d_countrytoid = {}
d_idtocountry = {}
with open('TradeGraphcsv.txt', 'r') as file:
    for line in file:
        vals = line.split('\t')[0]
        vals2 = line.split('\t')[1]
        countries.append(vals)
        countries.append(vals2)
    countries = set(countries)
    #countries.remove('...')
    countries = list(countries)
    countries.sort()
    d_idtocountry = {k: v for (k,v) in zip(range(1,len(countries)+1),countries)}
    d_countrytoid = {k: v for (k,v) in zip(countries,range(1,len(countries)+1))}

'''    
with open('TradeGraphcsv_id.txt', 'w') as file:
    with open('TradeGraphcsv.txt', 'r') as file2:
        for line2 in file2:
            vals = line2.split('\t')
            file.write(repr(d_countrytoid[vals[0]])+'\t'+repr(d_countrytoid[vals[1]])+'\t'+vals[2])
'''

with open('TradeGraphcsv_id.txt', 'r') as file:
    # Creating the graph, and storing the explicit trade volumes as weights
    G = nx.DiGraph()
    for line in file:
        vals = line.split()
        G.add_edge(int(vals[0]),int(vals[1]),weight = float(vals[2]))
    # G.out_degree(x, weight = 'weight') to ask for total out degree weight of a country x
    # G[u][v]['weight'] to get edge weight

with open('TradeData_Geometric_probabilities_cum.txt', 'r') as file:
    d_weights = {}
    for line in file:
        vals = line.split()   
        d_weights[(int(vals[0]),int(vals[1]))] = -log(float(vals[2]),2)
        
with open('TradeData_Geometric_probabilities_Agreements_USDOM.txt', 'r') as file:
    d_weights_USDOM = {}
    for line in file:
        vals = line.split()   
        d_weights_USDOM[(int(vals[0]),int(vals[1]))] = -log(float(vals[2]),2)

with open('TradeData_Geometric_probabilities_Agreements_VSCHINAGER.txt', 'r') as file:
    d_weights_VSCHINAGER= {}
    for line in file:
        vals = line.split()   
        d_weights_VSCHINAGER[(int(vals[0]),int(vals[1]))] = -log(float(vals[2]),2)
