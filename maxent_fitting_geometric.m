function [la,mu,jcols,jrows,errors,ps,probabilities]=maxent_fitting_geometric(D,nit)

% function [la,mu,jcols,jrows,errors,ps,probabilities]=maxent_fitting(D,nit)
%
%This is the maxent binary database (or bipartite graph) fitting function.

[n,m]=size(D);

prows=full(sum(D,2))/m;
pcols=full(sum(D,1))'/n;
%figure,spy(D)

% This method is a Quasi-Newton, with determination of step length.
% The fact that rows with same prows and columns with same pcols values
% will result in the same la and mu values is exploited.

[prowsunique,irows,jrows]=unique(prows);
[pcolsunique,icols,jcols]=unique(pcols);
nunique=length(prowsunique);
munique=length(pcolsunique);

la=-ones(munique,1);
mu=-ones(nunique,1);
errors=zeros(nit,1);
h=zeros(nunique+munique,1);

vcols=zeros(munique,1);
for i=1:munique
    vcols(i)=length(find(jcols==i));
end
vrows=zeros(nunique,1);
for i=1:nunique
    vrows(i)=length(find(jrows==i));
end

%tic
for k=1:nit-1
%    disp(['iteration ' num2str(k)])
    E=(ones(nunique,1)*exp(la)').*(exp(mu)*ones(1,munique));
    ps=E./(1-E);
    gla=-n*pcolsunique+ps'*vrows;
    gmu=-m*prowsunique+ps*vcols;
    errors(k)=norm([gla(jcols);gmu(jrows)]);
    
    H=zeros(munique+nunique,munique+nunique);
    H(munique+1:munique+nunique,1:munique)=E./(1-E).^2;
    H(1:munique,munique+1:munique+nunique)=H(munique+1:munique+nunique,1:munique)';
    h=H*[vcols ; vrows];
    
    deltalamu=-[gla;gmu]./h;
    
    fbest=0;
    errorbest=errors(k);
    for f=logspace(-5,1,100)
        latry=la+f*deltalamu(1:munique);
        mutry=mu+f*deltalamu(munique+1:munique+nunique);
        Etry=(ones(nunique,1)*exp(latry)').*(exp(mutry)*ones(1,munique));
        if max(max(Etry))<1
            pstry=Etry./(1-Etry);
            glatry=-n*pcolsunique+pstry'*vrows;
            gmutry=-m*prowsunique+pstry*vcols;
            errortry=norm([glatry(jcols);gmutry(jrows)]);
            if errortry<errorbest
                fbest=f;
                errorbest=errortry;
            end
        end
    end
%    disp(['factor: ' num2str(fbest)])
%    disp(['error: ' num2str(errorbest)])
    
    la=la+fbest*deltalamu(1:munique);
    mu=mu+fbest*deltalamu(munique+1:munique+nunique);
end
%toc

E=(ones(nunique,1)*exp(la)').*(exp(mu)*ones(1,munique));
ps=E./(1-E);
gla=-n*pcolsunique+ps'*vrows;
gmu=-m*prowsunique+ps*vcols;
errors(nit)=norm([gla(jcols);gmu(jrows)]);

if nargout>=7
    probabilities = 1-E(jrows,jcols);
end


%figure,imagesc(log(ps)),colorbar
%figure,imagesc(log(ps(jrows,jcols))),colorbar
%figure,plot(log(errors))

