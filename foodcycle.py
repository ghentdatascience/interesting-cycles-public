from CycleAlgos import *

G = nx.DiGraph()
with open('Florida_arcs.txt', 'r') as file:
    for line in file:
        vals = line.split()
        G.add_edge(int(vals[0]),int(vals[1]))

        
Grev = nx.reverse(G)
d_weights_geometric = {}
with open('Florida_geometricProbcum.txt', 'r') as file:
    for line in file:
        vals = line.split()
        d_weights_geometric[(int(vals[0]),int(vals[1]))] = -log(float(vals[2]))
