function y = obj_Geometric_Blocks(x, indicesDiag, l_r, l_c, occur_r, occur_c, d_r, d_c, ActualBinSums, ActualDiagonalSum)
% "Reduced"  Lagrange Dual for the DAG (bins) constraints. 
% Function of the reduced lambdas.
% x is a vector of length (sumlr+sumlc+2*numberofbins-1).

numberofbins = length(occur_r);
csumlr = cumsum(l_r);
sumlr = csumlr(numberofbins);
csumlc = cumsum(l_c);
sumlc = csumlc(numberofbins);

Ar = repmat(x(1:sumlr)', 1, sumlc);
Ac = repmat(x(sumlr+1:sumlr+sumlc), sumlr, 1);

MatrixAlpha = zeros(sumlr, sumlc);
startr = 1;
startc = 1;
for i=1:numberofbins-1
    MatrixAlpha(startr:csumlr(i), startc:csumlc(i)) = ones(csumlr(i)-startr+1, csumlc(i)-startc+1)*x(sumlr+sumlc+i);
    startr = csumlr(i)+1;
    startc = csumlc(i)+1;
end

MatrixDiag = zeros(sumlr, sumlc);
MatrixDiag(indicesDiag) = x(sumlr+sumlc+numberofbins-1+1);

MatrixOccurR = repmat(occur_r{1}, 1, sumlc); % Defining the Occurence matrices.
MatrixOccurC = repmat(occur_c{1}', sumlr, 1);
for i=2:numberofbins
    MatrixOccurR = [MatrixOccurR; repmat(occur_r{i}, 1, sumlc)];
    MatrixOccurC = [MatrixOccurC repmat(occur_c{i}', sumlr, 1)];
end

MatrixOccurD = zeros(sumlr, sumlc);
val = unique(indicesDiag);
MatrixOccurD(val) = histc(indicesDiag,val);

DR = occur_r{1}.*d_r{1};
DC = occur_c{1}.*d_c{1};
for i=2:numberofbins
    DR = [DR; occur_r{i}.*d_r{i}];
    DC = [DC; occur_c{i}.*d_c{i}];
end

DR = dot(x(1:sumlr), DR);
DC = dot(x(sumlr+1:sumlr+sumlc), DC);
som = -sum(sum((MatrixOccurR.*MatrixOccurC-MatrixOccurD).*log(1-exp(Ar+Ac+MatrixAlpha))));
som = som - sum(sum(MatrixOccurD.*log(1-exp(Ar+Ac+MatrixAlpha+MatrixDiag))));
som = som-DR-DC-dot(x(sumlr+sumlc+1:sumlr+sumlc+numberofbins-1), ActualBinSums)-x(sumlr+sumlc+numberofbins-1+1)*ActualDiagonalSum;
y = som;