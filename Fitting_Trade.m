clear;
clc;

fileID = fopen('TradeGraphcsv_id.txt', 'r');
Aorig = fscanf(fileID, '%i %i %f\n', [3 Inf]);
fclose(fileID);

numberofnodes = max(max([Aorig(1,:); Aorig(2,:)]));
values = Aorig(3,:); % Rounding the trade data to natural numbers -> geometric distributio
values = values./100;


A = sparse(Aorig(1,:), Aorig(2,:), values, numberofnodes, numberofnodes);

[la,mu,jcols,jrows,errors,ps,probabilities]=maxent_fitting_geometric(A,100);

% Writing the weights to a file.

from = Aorig(1,:);
to = Aorig(2,:);

la_from = mu(jrows(from));
la_to = la(jcols(to));

% This gives the exact weight Prob(X=x) in the geometric distribution
%weights = exp(la_from+la_to).^(values');
%weights = (1-exp(la_from+la_to)).*weights;

% This gives the cumulative weights Prob(X>=x) of the geometric
% distribution, i.e. Prob(X>=x) = (1-p)^x

p = 1-exp(la_from+la_to);
weights = (1-p).^(values');

fileID = fopen('TradeData_Geometric_probabilities_cum.txt', 'w');
fprintf(fileID,'%i %i %2.5e\r\n', [from; to; weights']); 
fclose(fileID);