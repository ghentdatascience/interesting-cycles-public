import networkx as nx
import random
import numpy as np
from math import log
from time import time
from networkx.utils import *
from collections import defaultdict
from collections import Counter
from itertools import combinations
from bisect import bisect_right

# nx.simple_cycles for generating all simple cycles (for experimental comparison)
# nx.erdos_renyi_graph for generating random directed graphs
        
def weighted_shuffle(a_list,w_list):
    a = np.array(a_list)
    w = np.array(w_list)
    r = np.empty_like(a)
    cumWeights = np.cumsum(w)
    for i in range(len(a)):
         rnd = random.random() * cumWeights[-1]
         j = bisect_right(cumWeights,rnd)
         #j = np.searchsorted(cumWeights, rnd, side='right')
         r[i]=a[j]
         cumWeights[j:] -= w[j]
    return list(r)


def MinMeanCycle_Karp(G, d_weights):
    ### Karp's Algorithm for finding the min. mean weight cycle ###
    ### max. mean weight cycle can be found by flipping weights ###
    if type(G) != nx.classes.digraph.DiGraph:
        print('Graph is not directed!')
        return
    
    # Preprocessing G.
    for n in G.nodes():
        G.add_edge('source', n)
        d_weights[('source', n)]=0

    n = len(G.nodes())


    F = {(k, x): 100000 for k in range(0,n+1) for x in G.nodes()}
    F[(0,'source')]=0
    P = {(k, x): None for k in range(1,n+1) for x in G.nodes()}

    for k in range(1,n+1):
        for x in G.nodes():
            for w in G.predecessors(x):
                if F[(k-1, w)] + d_weights[(w, x)] < F[(k, x)]:
                    F[(k, x)] = F[(k-1, w)] + d_weights[(w, x)]
                    P[(k, x)] = w
    
    y = 100000
    winnerVertex = None
    for x in (x for x in G.nodes() if F[(n,x)] != 100000): 
        kand = max([(F[(n,x)]-F[(k,x)])/(n-k) for k in range(0,n) if F[(k,x)]<100000])
        if kand<y:
            y = kand
            winnerVertex = x

    # Restoring G.
    G.remove_node('source')
    for x in G.nodes():  
        del d_weights[('source',x)]

    if winnerVertex == None:
        return 'G is acyclic!'

    # Finding the first cycle in the edge progression of winnerVertex
    edgeProgression = [winnerVertex]
    x = winnerVertex
    for k in range(0,n):
        if P[(n-k,x)] not in edgeProgression:
            x = P[(n-k,x)]
            edgeProgression.insert(0, x)
        else:
            x = P[(n-k,x)]
            ind = edgeProgression.index(x)
            edgeProgression.insert(0, x)
            break
    cycle = edgeProgression[0:ind+2]    
    return cycle

def MaxMeanCycle_Karp(G, d_weights2):
    ### Karp's Algorithm for finding the min. mean weight cycle ###
    ### max. mean weight cycle can be found by flipping weights ###
    d_weights = { k: -v for k, v in d_weights2.items()}
    cycle = MinMeanCycle_Karp(G, d_weights)
    return cycle

def Karp_Variant1_Heuristic(G, q, d_weights2):
    ### A variant of Karp's Algorithm for finding the min. mean weight cycle, used as a heuristic for MSIC. ###
    ### We use the top 5 nodes from min_{v} max_{k} (F_n(x)-F_k(x)) / (alpha*(n-k)+beta*n), and find the cycles from F_n(x), and then pick the best scoring one.
    if type(G) != nx.classes.digraph.DiGraph:
        print('Graph is not directed!')
        return
    
    # Preprocessing G.
    d_weights = { k: -v for k, v in d_weights2.items()}
    for n in G.nodes():
        G.add_edge('source', n)
        d_weights[('source', n)]=0
        
    # Initialize
    n = len(G.nodes())
    alpha = log((1-q)/q,2)
    beta = n*log(1/(1-q),2)
    F = {(k, x): 100000 for k in range(0,n+1) for x in G.nodes()}
    F[(0,'source')]=0
    P = {(k, x): None for k in range(1,n+1) for x in G.nodes()}

    # Compute F, and P
    for k in range(1,n+1):
        for x in G.nodes():
            for w in G.predecessors(x):
                if F[(k-1, w)] + d_weights[(w, x)] < F[(k, x)]:
                    F[(k, x)] = F[(k-1, w)] + d_weights[(w, x)]
                    P[(k, x)] = w
    
    top5_winners = set()
    for i in range(0,1):
        y = 100000
        winnerVertex = None
        for x in (x for x in G.nodes() if F[(n,x)] != 100000 and x not in top5_winners): 
            kand = max([(F[(n,x)]-F[(k,x)])/(alpha*(n-k)+beta) for k in range(0,n) if F[(k,x)]<100000])
            if kand<y:
                y = kand
                winnerVertex = x
        if winnerVertex != None:
            top5_winners.add(winnerVertex)

    # Restoring G.
    G.remove_node('source')
    for x in G.nodes():  
        del d_weights[('source',x)]   
        
    if not top5_winners:
        return 'G is acyclic!'

    mini = 100000
    for i in top5_winners:
        edgeProgression = [i]
        x = i
        for k in range(0,n):
            if P[(n-k,x)] not in edgeProgression:
                x = P[(n-k,x)]
                edgeProgression.insert(0, x)
            else:
                x = P[(n-k,x)]
                ind = edgeProgression.index(x)
                edgeProgression.insert(0, x)
                break
        cycle = edgeProgression[0:ind+2]
        SI_cycle = IC_Cycle(cycle,d_weights)/(alpha*(len(cycle)-1)+beta) 
        if SI_cycle < mini:
            best_cycle = cycle
            mini = SI_cycle
    return best_cycle

def FixedNodeKarp_Heuristic1(G, weights, v):
    ### A first heuristic for finding the maximum mean cycle through a fixed node v.
    ### We sufficiently increase the weights of all outgoing edges of v, and then apply MaxMeanCycle_Karp.
    maxEdge = max(weights.values())
    
    d_weights_original = {}
    for e in G.out_edges(v):
        d_weights_original[(e[0],e[1])] = weights[(e[0],e[1])]
    
    vInCycle = False
    teller = 0 
    while not vInCycle:
        teller += 1
        for e in G.out_edges(v):
            weights[(e[0],e[1])] = weights.get((e[0],e[1]), 0) + maxEdge
        cycle = MaxMeanCycle_Karp(G, weights)
        if v in cycle:
            vInCycle = True
        if teller > 30:
            weights.update(d_weights_original)
            return 'Probably no cycle exists'
            
    weights.update(d_weights_original)
    return cycle

def FixedNodeKarp_Heuristic2(G, probabilities, v):
    ### A second heuristic for finding the maximum mean cycle through a fixed node v.
    ### This heuristic uses a shortest path from v->v with weights = probabilities (i.e. using highly surprising and short paths)
    for i,j,d in G.edges(data=True):
        d['weight'] = probabilities[(i,j)]
        
    minSom = 100000
    mincycle = 'v is not contained in a cycle'
    for n in G.predecessors(v):
        som = 0
        try:
            path = nx.shortest_path(G, v, n, weight='weight')
            som += G[n][v]['weight']
        except:
            path = []# no path between v and n 
            som = 100000
        for x, y in zip(path[:-1],path[1:]):
            som += G[x][y]['weight']
        if som<minSom:
            minSom = som
            mincycle = path + [v]
    return mincycle

def SamplesCycles(G, samplesize):
    ### Generating a samplesize of cycles
    stack = []
    teller = 0
    for c in nx.simple_cycles(G):
        stack.append(c+[c[0]])
        teller+=1
        if teller>=samplesize:
            break
    return stack
        

def SamplesCyclesFixedNode(G, samplesize, v):
    ### Generating a samplesize of cycles through a node
    stack = []
    teller = 0
    teller2 = 1
    for c in nx.simple_cycles(G):
        teller2+=1
        if v in c:
            stack.append(c+[c[0]])
            teller+=1
        if teller>=samplesize:
            break
        if teller2>=1000000:
            break
    return stack

def IC_Cycle(cycle, d_weights):
    # assume cycle is in the form of e.g. [2, 3, 44, 2]
    ic = 0
    for x, y in zip(cycle[:-1],cycle[1:]):
        ic += d_weights[(x,y)]
    return ic

def PerformanceHeuristic(G, q, probabilities, maxlength, iterations):
    # Here we test the performance of Heur1 and Heur2, by comparing with the optimal cycle over a pool of cycles
    # For tests on small random graphs, use:
    ## G = nx.erdos_renyi_graph(20, 0.2, directed=True)
    ## probs = {}
    ## for e in G.edges():
    ##        probs[(e[0],e[1])] = random.uniform(0,1)
    
    d_weights = {}
    for e in probabilities:
        d_weights[e] = -log(probabilities[e],2)
        
    alpha = log((1-q)/q,2)
    beta = log(1/(1-q),2)
    max_samplesize = 1000000 # the max. number of cycles through a node we consider
    teller = 0
    while iterations > teller:
        v = random.choice(G.nodes())
        #pool = SamplesCyclesFixedNode(G, max_samplesize, v)
        t0 = time()
        cycles = list(simple_cycles_fixedNode_maxLength(G, v, maxlength))
        if cycles:
            teller += 1
            si = [IC_Cycle(c+[c[0]], d_weights)/(alpha*len(c)+beta) for c in cycles]
            top1 =  np.argsort(si)[-1:]
            ind = top1[0]
            heur3a_si = si[ind]
            t1 = time()

            t_h3a = round(t1-t0,2)

            t0 = time()
            cycles = list(simple_cycles_fixedNode_maxLength(G, v, maxlength+1))
            si = [IC_Cycle(c+[c[0]], d_weights)/(alpha*len(c)+beta) for c in cycles]
            top1 =  np.argsort(si)[-1:]
            ind = top1[0]
            heur3b_si = si[ind]
            t1 = time()

            t_h3b = round(t1-t0,2)

            t0 = time()
            cycles = list(simple_cycles_fixedNode_maxLength(G, v, maxlength+2))
            si = [IC_Cycle(c+[c[0]], d_weights)/(alpha*len(c)+beta) for c in cycles]
            top1 =  np.argsort(si)[-1:]
            ind = top1[0]
            heur3c_si = si[ind]
            t1 = time()

            t_h3c = round(t1-t0,2)
            
            #l = [IC_Cycle(x, d_weights)/(alpha*(len(x)-1)+beta) for x in pool]
            #bestSI = max(l)
            #bestSIindex = l.index(bestSI)
            #bestSIcycle = pool[bestSIindex]          

            t0 = time()
            h1 = FixedNodeKarp_Heuristic1(G, d_weights, v)
            t1= time()
            t_h1 = round(t1-t0,2)
            
            t0 = time()
            h2 = FixedNodeKarp_Heuristic2(G, probabilities, v)
            t1= time()
            t_h2 = round(t1-t0,2)

            SI_h1 = IC_Cycle(h1, d_weights)/(alpha*(len(h1)-1)+beta)
            SI_h2 = IC_Cycle(h2, d_weights)/(alpha*(len(h2)-1)+beta)

            with open('3HeuristicsSI_Trade.txt', 'a') as file:
                #file.write(repr(SI_h1/bestSI)+'\t'+repr(SI_h2/bestSI)+'\t'+repr(sum(l)/len(l)/bestSI)+'\t'+repr(len(pool))+'\n')
                file.write(repr(SI_h1)+'\t'+repr(SI_h2)+'\t'+repr(heur3a_si)+'\t'+repr(heur3b_si)+'\t'+repr(heur3c_si)+'\n')
            with open('3HeuristicsTime_Trade.txt', 'a') as file:
                #file.write(repr(SI_h1/bestSI)+'\t'+repr(SI_h2/bestSI)+'\t'+repr(sum(l)/len(l)/bestSI)+'\t'+repr(len(pool))+'\n')
                file.write(repr(t_h1)+'\t'+repr(t_h2)+'\t'+repr(t_h3a)+'\t'+repr(t_h3b)+'\t'+repr(t_h3c)+'\n')
                
            print(teller)



def simple_cycles_fixedNode(G, v):
    """
    Find simple cycles (elementary circuits) of a directed graph CONTAINING A FIXED NODE.

    A simple cycle, or elementary circuit, is a closed path where no
    node appears twice, except that the first and last node are the same.
    Two elementary circuits are distinct if they are not cyclic permutations
    of each other.

    Parameters
    ----------
    G : NetworkX DiGraph
       A directed graph

    Returns
    -------
    cycle_generator: generator
       A generator that produces elementary cycles of the graph.  Each cycle is
       a list of nodes with the first and last nodes being the same.
    """
    def _unblock(thisnode,blocked,B):
        stack=set([thisnode])
        while stack:
            node=stack.pop()
            if node in blocked:
                blocked.remove(node)
                stack.update(B[node])
                B[node].clear()

    for g in nx.strongly_connected_component_subgraphs(G):
        if v in g:
            subG = g
            break
    startnode = v

    # Processing node runs "circuit" routine from recursive version
    path=[startnode]
    blocked = set() # vertex: blocked from search?
    closed = set() # nodes involved in a cycle
    blocked.add(startnode)
    B=defaultdict(set) # graph portions that yield no elementary circuit
    stack=[ (startnode,list(subG[startnode])) ]  # subG gives component nbrs
    while stack:
        thisnode,nbrs = stack[-1]
        if nbrs:
            nextnode = nbrs.pop()
            if nextnode == startnode:
                yield path[:]
                closed.update(path)
                
            elif nextnode not in blocked:
                path.append(nextnode)
                stack.append( (nextnode,list(subG[nextnode])) )
                closed.discard(nextnode)
                blocked.add(nextnode)
                continue
        if not nbrs:  # no more nbrs
            if thisnode in closed:
                _unblock(thisnode,blocked,B)
            else:
                for nbr in subG[thisnode]:
                    if thisnode not in B[nbr]:
                        B[nbr].add(thisnode)
            stack.pop()
            path.pop()
        # done processing this node

def simple_cycles_fixedNode_maxLength(G, v, query, maxlength):

    Steiner_cycle = 'No cycle of maxlength exists between the query nodes'
    if maxlength<len(query):
        return Steiner_cycle

    Gpruned = G
    Grevpruned = nx.reverse(G)
    for q in (n for n in query if n in Gpruned):
        d_to_x = nx.single_source_shortest_path_length(Gpruned,q,maxlength)
        d_from_x = nx.single_source_shortest_path_length(Grevpruned,q,maxlength)
        valid_nodes = [n for n in d_to_x if n in d_from_x and d_to_x[n]+d_from_x[n] <= maxlength]
        Gpruned = Gpruned.subgraph(valid_nodes)
        Grevpruned = Grevpruned.subgraph(valid_nodes)

    if not all(n in Gpruned.nodes() for n in query):
            return Steiner_cycle

    def _unblock(thisnode,blocked,B):
        stack=set([thisnode])
        while stack:
            node=stack.pop()
            if node in blocked:
                blocked.remove(node)
                stack.update(B[node])
                B[node].clear()

    subG = Gpruned
    startnode = v

    # Processing node runs "circuit" routine from recursive version
    path=[startnode]
    blocked = set() # vertex: blocked from search?
    closed = set() # nodes involved in a cycle
    blocked.add(startnode)
    B=defaultdict(set) # graph portions that yield no elementary circuit
    stack=[ (startnode,list(subG[startnode])) ]  # subG gives component nbrs
    while stack:
        thisnode,nbrs = stack[-1]
        if nbrs:
            nextnode = nbrs.pop()
            if nextnode == startnode:
                yield path[:]
                closed.update(path)
                
            elif nextnode not in blocked:
                path.append(nextnode)
                stack.append( (nextnode,list(subG[nextnode])) )
                closed.discard(nextnode)
                blocked.add(nextnode)
                continue
        if not nbrs:  # no more nbrs
            if thisnode in closed:
                _unblock(thisnode,blocked,B)
            else:
                for nbr in subG[thisnode]:
                    if thisnode not in B[nbr]:
                        B[nbr].add(thisnode)
            stack.pop()
            path.pop()
        # done processing this node

def find_Steiner_Cycle(G, Grev, query, maxlength):
    # This function tries to find a Steiner cycle with maxlength going through the query nodes.
    # First, we prune down the graph, to nodes that are within reach of ALL the query nodes, and can reach ALL the query nodes
    # The heuristic is the search is the distance TOWARDS all query nodes

    Steiner_cycle = 'No cycle of maxlength exists between the query nodes'
    if maxlength<len(query):
        return Steiner_cycle
    
    ## For any potential node x in the Steiner cycle, we have SP(q_i -> x)+ SP(x -> q_i) <= maxlength
    Gpruned = G
    Grevpruned = Grev
    for q in (n for n in query if n in Gpruned):
        d_to_x = nx.single_source_shortest_path_length(Gpruned,q,maxlength)
        d_from_x = nx.single_source_shortest_path_length(Grevpruned,q,maxlength)
        valid_nodes = [n for n in d_to_x if n in d_from_x and d_to_x[n]+d_from_x[n] <= maxlength]
        Gpruned = Gpruned.subgraph(valid_nodes)
        Grevpruned = Grevpruned.subgraph(valid_nodes)

    if not all(n in Gpruned.nodes() for n in query):
            return Steiner_cycle

    # We define a Total_Distance dict., used in our search. Nodes that have a total shortest path TOWARDS all query nodes are preferred.
    total_distance = {k: 0 for k in Gpruned}
    for q in query:
        d1 = nx.shortest_path_length(Grevpruned, source = q)
        d2 = nx.shortest_path_length(Gpruned, source = q)
        d = {k:d1[k]+d2[k] for k in Gpruned}
        total_distance = {k:total_distance[k]+d[k] for k in Gpruned}
    total_distance = total_distance
    
    # Next we run a DFS search, prioritizing nodes that are closer to the query nodes.
    source = query[0]
    visited = [source]
    d = dict({k:total_distance[k] for k in Gpruned[source]})
    neighbors_sorted = sorted(d, key = d.get, reverse=False)
    stack = [iter(neighbors_sorted)]
    while stack:
        children = stack[-1]
        child = next(children, None)
        if child is None:
            stack.pop()
            visited.pop()
        elif len(visited) < maxlength:
            if (child == source) and set([x for x in query if x is not source]).issubset(visited):
                return visited + [source]
            elif child not in visited:
                visited.append(child)
                d = dict({k:total_distance[k] for k in Gpruned[child]})
                neighbors_sorted = sorted(d, key = d.get, reverse=False)
                stack.append(iter(neighbors_sorted))
        else: #len(visited) == maxlength:
            if child == source or source in children:
                if set([x for x in query if x is not source]).issubset(visited):
                    return visited + [source]
            stack.pop()
            visited.pop()

    return Steiner_cycle

def find_Steiner_Cycle_random(G, query, maxlength):
    # This function tries to find a Steiner cycle with maxlength going through the query nodes.
    # First, we prune down the graph, to nodes that are within reach of ALL the query nodes, and can reach ALL the query nodes
    # The heuristic is the search is the distance TOWARDS all query nodes
    Grev = nx.reverse(G)
    Steiner_cycle = 'No cycle of maxlength exists between the query nodes'
    if maxlength<len(query):
        return Steiner_cycle
    
    ## For any potential node x in the Steiner cycle, we have SP(q_i -> x)+ SP(x -> q_i) <= maxlength
    Gpruned = G
    Grevpruned = Grev
    for q in (n for n in query if n in Gpruned):
        d_to_x = nx.single_source_shortest_path_length(Gpruned,q,maxlength)
        d_from_x = nx.single_source_shortest_path_length(Grevpruned,q,maxlength)
        valid_nodes = [n for n in d_to_x if n in d_from_x and d_to_x[n]+d_from_x[n] <= maxlength]
        Gpruned = Gpruned.subgraph(valid_nodes)
        Grevpruned = Grevpruned.subgraph(valid_nodes)

    if not all(n in Gpruned.nodes() for n in query):
            return Steiner_cycle

    # We define a Total_Distance dict., used in our search. Nodes that have a total shortest path TOWARDS all query nodes are preferred.
    total_distance = {k: 0 for k in Gpruned}
    for q in query:
        d = nx.shortest_path_length(Grevpruned, source = q)
        total_distance = {k:total_distance[k]+d[k] for k in Gpruned}
    
    # Next we run a DFS search, prioritizing nodes that are closer to the query nodes.
    source = random.choice(query)
    visited = [source]
    weights = [1/max(total_distance[k],0.01)/max(Gpruned.out_degree(k),1) for k in Gpruned[source]]
    weights = [x**3 for x in weights]
    weights = [w/sum(weights) for w in weights]
    neighbors_shuffled = list(np.random.choice(list(Gpruned[source]), len(Gpruned[source]), replace = False, p = weights)) 
    stack = [iter(neighbors_shuffled)]
    while stack:
        children = stack[-1]
        child = next(children, None)
        if child is None:
            stack.pop()
            visited.pop()
        elif len(visited) < maxlength:
            if (child == source) and set([x for x in query if x is not source]).issubset(visited):
                return visited + [source]
            elif child not in visited:
                visited.append(child)
                weights = [1/max(total_distance[k],0.01)/max(Gpruned.out_degree(k),1) for k in Gpruned[child]]
                weights = [x**3 for x in weights]
                weights = [w/sum(weights) for w in weights]
                neighbors_shuffled = list(np.random.choice(Gpruned.successors(child), len(Gpruned[child]), replace = False, p = weights))
                stack.append(iter(neighbors_shuffled))
        else: #len(visited) == maxlength:
            if child == source or source in children:
                if set([x for x in query if x is not source]).issubset(visited):
                    return visited + [source]
            stack.pop()
            visited.pop()

    return Steiner_cycle


def find_Steiner_Cycle_Johnson(G, query, maxlength):
    resulting_cycle = 'No cycle of maxlength exists between the query nodes'
    for c in simple_cycles_fixedNode_maxLength(G, query[0], query, maxlength):
        if set(query).issubset(set(c)) and len(c) <= maxlength:
            resulting_cycle = c
            break
    if resulting_cycle == 'No cycle of maxlength exists between the query nodes':
        return resulting_cycle
    else:
        return resulting_cycle + [resulting_cycle[0]]

def find_Steiner_Cycle_random2(G, query, maxlength):
    # An alternative (faster) way of finding random initial solutions, by performing random changes on ini cycle.
    query2 = sorted(query, key=G.degree)
    Grev = nx.reverse(G)
    ini_cycle = find_Steiner_Cycle(G, Grev, query2, maxlength)
    if ini_cycle == 'No cycle of maxlength exists between the query nodes':
        return ini_cycle
    d_weights_uni = {}
    for e in G.edges():
        d_weights_uni[e] = 1
    alpha = 1
    beta = 1
    max_tries = 20
    Steiner_cycle = random_cascade_heuristics(G, ini_cycle, query2, alpha, beta, d_weights_uni, max_tries, maxlength)
    return Steiner_cycle

def random_shortcut_cycle(G, cycle, query):
    # Here we look for a random shortcut
    # We iterate over all nodes, and keep the best one.
    shortcut_cycle = cycle
    l = list(np.random.choice(cycle[:-1], len(cycle[:-1]), replace = False))
    for n in l:
        idx = cycle.index(n)
        permuted_cycle = cycle[idx:-1]+cycle[0:(idx+1)]
        teller = 1
        viable_shortcut = True
        while viable_shortcut:
            endpoint_cand = permuted_cycle[teller]
            if (n,endpoint_cand) in G.edges():
                shortcut_cycle = [n]+[endpoint_cand]+permuted_cycle[(teller+1):]
                if set(shortcut_cycle) != set(cycle):
                    return shortcut_cycle
            if endpoint_cand in query:
                viable_shortcut = False
            teller += 1
    return shortcut_cycle

def random_extending_cycle(G, cycle):
    # Here we find a random "Extending Change"
    l = list(np.random.choice(cycle[:-1], len(cycle[:-1]), replace = False))
    for n in l:
        idx = cycle.index(n)
        j_list = []
        for j in (x for x in G.successors(n) if x in G.predecessors(cycle[idx+1]) and x not in cycle):
            j_list.append(j)
        if j_list:
            j_pick = random.choice(j_list)
            cand_cycle = cycle[:idx+1]+[j_pick]+cycle[idx+1:]
            return cand_cycle
    return cycle

def shortcut_cycle(G, cycle, query, alpha, beta, d_weights):
    # Here we look for the best 'shortcut' of one or more edges in a given Steiner cycle.
    # We iterate over all nodes, and keep the best one.
    SIR_best = IC_Cycle(cycle, d_weights)/(alpha*(len(cycle)-1)+beta)
    shortcut_cycle = cycle
    for n in cycle[:-1]:
        idx = cycle.index(n)
        permuted_cycle = cycle[idx:-1]+cycle[0:(idx+1)]
        teller = 1
        viable_shortcut = True
        while viable_shortcut:
            endpoint_cand = permuted_cycle[teller]
            if (n,endpoint_cand) in G.edges():
                cand_cycle = [n]+[endpoint_cand]+permuted_cycle[(teller+1):]
                cand_SI = IC_Cycle(cand_cycle, d_weights)/(alpha*(len(cand_cycle)-1)+beta)
                if cand_SI >= SIR_best:
                    shortcut_cycle = cand_cycle
                    SIR_best = cand_SI
            if endpoint_cand in query:
                viable_shortcut = False
            teller += 1
    return shortcut_cycle

def Three_CHANGE(G, cycle, alpha, beta, d_weights):
    # Here we find the best "Sequantial Primary Change", i.e. performing a 3-change on the cycle, see paper Kanellakis about TSP.
    opt_cycle = cycle
    
    if len(cycle) <= 3:
        return opt_cycle
    score_best = IC_Cycle(cycle, d_weights)/(alpha*(len(cycle)-1)+beta)
    
    edgeList = [(x,y) for x,y in zip(cycle[:-1],cycle[1:])]
    for e1,e2,e3 in combinations(edgeList, 3): # Finding 3 edges, as they are present in the cycle
        if (e1[0],e2[1]) in G.edges() and (e2[0],e3[1]) in G.edges() and (e3[0],e1[1]) in G.edges():
            id1a = cycle.index(e1[0])
            id1b = cycle.index(e1[1])
            id2a = cycle.index(e2[0])
            id2b = cycle.index(e2[1])
            id3a = cycle.index(e3[0])
            id3b = cycle.index(e3[1])
            if id3b == 0:
                id3b = len(cycle)-1
            cand_cycle =  cycle[0:id1a+1]+cycle[id2b:id3a+1]+cycle[id1b:id2a+1]+cycle[id3b:]
            kand_score = IC_Cycle(cand_cycle, d_weights)/(alpha*(len(cand_cycle)-1)+beta)
            if kand_score >= score_best:
                score_best = kand_score
                opt_cycle = cand_cycle
    return opt_cycle

def Quad_CHANGE(G, cycle, alpha, beta, d_weights):
    # Here find the best "Quad Change", see paper Kanellakis about TSP.
    opt_cycle = cycle
    if len(cycle) <= 4:
        return opt_cycle
    score_best = IC_Cycle(cycle, d_weights)/(alpha*(len(cycle)-1)+beta)

    edgeList = [(x,y) for x,y in zip(cycle[:-1],cycle[1:])]
    for e1,e2,e3,e4 in combinations(edgeList, 4): # Finding 3 edges, as they are present in the cycle
        if (e1[0],e3[1]) in G.edges() and (e2[0],e4[1]) in G.edges() and (e3[0],e1[1]) in G.edges() and (e4[0],e2[1]) in G.edges():
            id1a = cycle.index(e1[0])
            id1b = cycle.index(e1[1])
            id2a = cycle.index(e2[0])
            id2b = cycle.index(e2[1])
            id3a = cycle.index(e3[0])
            id3b = cycle.index(e3[1])
            id4a = cycle.index(e4[0])
            id4b = cycle.index(e4[1])
            if id4b == 0:
                id4b = len(cycle)-1
            cand_cycle =  cycle[0:id1a+1]+cycle[id3b:id4a+1]+cycle[id2b:id3a+1]+cycle[id1b:id2a+1]+cycle[id4b:]
            kand_score = IC_Cycle(cand_cycle, d_weights)/(alpha*(len(cand_cycle)-1)+beta)
            if kand_score >= score_best:
                score_best = kand_score
                opt_cycle = cand_cycle

    return opt_cycle

def Extending_Change(G, cycle, alpha, beta, d_weights):
    # Here we find the best "Extending Change", i.e. we bypass an edge by adding two other edges.
    opt_cycle = cycle
    score_best = IC_Cycle(cycle, d_weights)/(alpha*(len(cycle)-1)+beta)
    for n in cycle[:-1]:
        idx = cycle.index(n)
        for j in (x for x in G.successors(n) if x in G.predecessors(cycle[idx+1]) and x not in cycle):
            cand_cycle = cycle[:idx+1]+[j]+cycle[idx+1:]
            kand_score = IC_Cycle(cand_cycle, d_weights)/(alpha*(len(cand_cycle)-1)+beta)
            if kand_score >= score_best:
                score_best = kand_score
                opt_cycle = cand_cycle
    return opt_cycle
            

def compareKarpVsForsied(iterations, q, size_graph):
    # A test on random graphs to see how good Karp scores on the FORSIED objective function.
    # q is a parameter to describe alpha and beta (DL)
    alpha = log((1-q)/q,2)
    beta = size_graph*log(1/(1-q),2)
    for n in range(1,iterations+1):
        G = nx.erdos_renyi_graph(size_graph, 0.2, directed=True) # creating random Erdos graph
        d_weights = {}
        for e in G.edges():
            d_weights[(e[0],e[1])] = random.randint(1,10000) # random (positive) weights
            
        maxSI = 0
        maxSICycle = []
        tellerCycles = 0
        avgSI = 0
        for n in nx.simple_cycles(G):
            tellerCycles +=1 
            numberofEdges = len(n)
            n2 = n[1::]
            n2.append(n[0])
            teller = sum([d_weights[(x, y)] for x, y in zip(n, n2)])
            check_SI = teller/(alpha*numberofEdges+beta)
            avgSI = avgSI*(tellerCycles-1)/tellerCycles + check_SI/tellerCycles
            if  check_SI > maxSI:
                maxSI = check_SI
                maxSICycle = n
                n_f = numberofEdges

        karp = MaxMeanCycle_Karp(G, d_weights)
        teller = sum([d_weights[(x, y)] for x, y in zip(karp[0:-1], karp[1::])])
        n_k = len(karp)-1

        SIKarp = teller/(alpha*n_k+beta)

        karp_variant = Karp_Variant1_Heuristic(G, q, d_weights)

        SI_variant = IC_Cycle(karp_variant, d_weights)/(alpha*(len(karp_variant)-1)+beta)

        with open('Karp_Variant_Test_Erdos_n20_q0.2.txt', 'a') as file:
            file.write(repr(SIKarp/maxSI)+'\t'+repr(SI_variant/maxSI)+'\t'+repr(avgSI/maxSI)+'\t'+repr(n_k*(alpha*n_f+beta)/n_f/(alpha*n_k+beta))+'\t'+repr(tellerCycles)+'\n')


def random_cascade_heuristics(G, ini_cycle, query, alpha, beta, d_weights, max_tries, maxlength):
    # Perform a random cascading of operations, a number of times
    cycle = ini_cycle
    teller = 0
    while teller < max_tries:
        i = random.choice(range(1,7))
        cycle_prev = cycle
        if i == 1:
            cycle = Three_CHANGE(G, cycle_prev, alpha, beta, d_weights)
        if i == 2:
            cycle = Quad_CHANGE(G, cycle_prev, alpha, beta, d_weights)
        if i == 3 or i == 5 or i == 6:
            cycle_test = random_extending_cycle(G, cycle_prev)
            if len(cycle_test) <= maxlength + 1:
                cycle = cycle_test
        if i == 4:
            cycle = random_shortcut_cycle(G, cycle_prev, query)
        teller +=1
    return cycle

def pickbest_cascade_heuristics(G, ini_cycle, query, alpha, beta, d_weights):
    # Perform a random cascading of operations, until no improvement can be done
    cycle = ini_cycle
    changes = True
    while changes:
        si_before = IC_Cycle(cycle,d_weights)/(alpha*(len(cycle)-1)+beta)
        cycle1 = Three_CHANGE(G, cycle, alpha, beta, d_weights)
        cycle3 = shortcut_cycle(G, cycle, query, alpha, beta, d_weights)
        #cycle4 = Extending_Change(G, cycle, alpha, beta, d_weights)
        cycle2 = Quad_CHANGE(G, cycle, alpha, beta, d_weights)
        S1 = IC_Cycle(cycle1,d_weights)/(alpha*(len(cycle1)-1)+beta)
        S2 = IC_Cycle(cycle2,d_weights)/(alpha*(len(cycle2)-1)+beta)
        S3 = IC_Cycle(cycle3,d_weights)/(alpha*(len(cycle3)-1)+beta)
        #S4 = IC_Cycle(cycle4,d_weights)/(alpha*(len(cycle4)-1)+beta)

        a = [S1,S2,S3]#,S4]
        index_max = a.index(max(a))
        if index_max == 0:
            cycle = cycle1
        elif index_max == 1:
            cycle = cycle2
        elif index_max == 2:
            cycle = cycle3
        else:
            cycle = cycle4
        if abs((max(a)-si_before)) < 0.001:
            changes = False

    return cycle

def Heuristic_Steiner(G, number_of_tries, q, query, d_weights, maxlength):
    # This finds an interesting Steiner cycle, up to a maxlength.
    # First find a ini_cycle with length <= maxlength, then proceed to add edges with Extending_Change until maxlength.
    # Afterwards, apply the best short_cut, quad or sequential change.
    # Repeat a number_of_tries time, and pick best result.
    alpha = log((1-q)/q,2)
    beta = len(G)*log(1/(1-q),2)
    SI_best = 0
    SI_best_ini_cycle = 0
    cycle_best = 'No cycle of maxlength exists between the query nodes'
    for k in range(1,number_of_tries+1):
        ini_cycle = find_Steiner_Cycle_random2(G, query, maxlength)
        if ini_cycle == 'No cycle of maxlength exists between the query nodes':
            return ini_cycle, None, None 
        else:
            SI_ini = IC_Cycle(ini_cycle,d_weights)/(alpha*(len(ini_cycle)-1)+beta)
            if SI_ini > SI_best_ini_cycle:
                    SI_best_ini_cycle = SI_ini
            
            length_extensions = maxlength-len(ini_cycle)+1
            for j in range(1,length_extensions+1):
                ini_cycle = Extending_Change(G, ini_cycle, alpha, beta, d_weights)
          
            cycle = pickbest_cascade_heuristics(G, ini_cycle, query, alpha, beta, d_weights)
            SI_cascade = IC_Cycle(cycle,d_weights)/(alpha*(len(cycle)-1)+beta)
            if SI_cascade > SI_best:
                SI_best = SI_cascade
                cycle_best = cycle
                
    return cycle_best, SI_best, SI_best_ini_cycle

def LocalSearch_Performance_Erdos(iterations, q, query, size_graph):
    # Testing the performance (comparing with OPT) of Heuristic Steiner
    alpha = log((1-q)/q,2)
    beta = size_graph*log(1/(1-q),2)
    tellerFailcycles = 0
    for n in range(1,iterations+1):
        G = nx.erdos_renyi_graph(size_graph, 0.2, directed=True)
        Grev = nx.reverse(G)
        d_weights = {}
        for e in G.edges():
            d_weights[(e[0],e[1])] = random.randint(1,1000)
        maxSI = 0
        maxSICycle = []
        tellerCycles = 0
        
        for c in simple_cycles_fixedNode(G, query[0]):
            if set(query).issubset(set(c)):
                tellerCycles +=1
                n2 = c+[c[0]]
                kand_SI = IC_Cycle(n2, d_weights)/(alpha*(len(n2)-1)+beta)
                if kand_SI > maxSI:
                    maxSI = kand_SI
                    maxSICycle = n2
        
        t0 = time()
        cycle_best, SI_best, SI_best_ini_cycle = Heuristic_Steiner(G, 5, q, query, d_weights, 5*len(query))
        t1 = time()
        
        if maxSI > 0 and SI_best:
            print(repr(SI_best_ini_cycle/maxSI)+'\t'+repr(SI_best/maxSI))
            with open('SteinerTestQ10_yes.txt', 'a') as file:
                file.write(repr(SI_best_ini_cycle/maxSI)+'\t'+repr(SI_best/maxSI)+'\t'+repr(round(t1-t0,5))+'\n')
        else:
            print('No Cycle found')
            with open('SteinerTestQ10_no.txt', 'a') as file:
                file.write(repr(round(t1-t0,5))+'\n')

def LocalSearch_Scalability_Erdos(iterations, q, query, size_graph, file_yes):
    # Testing the influence of the size of the Erdos graph on the running time
    alpha = log((1-q)/q,2)
    beta = size_graph*log(1/(1-q),2)
    tellerFailcycles = 0
    for n in range(1,iterations+1):
        G = nx.erdos_renyi_graph(size_graph, 0.2, directed=True)
        Grev = nx.reverse(G)
        d_weights = {}
        for e in G.edges():
            d_weights[(e[0],e[1])] = random.randint(1,1000)
               
        t0 = time()
        cycle_best, SI_best, SI_best_ini_cycle = Heuristic_Steiner(G, 1, q, query, d_weights, 8)
        t1 = time()
        
        if SI_best:
            with open(file_yes, 'a') as file:
                file.write(repr(round(t1-t0,5))+'\n')

def runScalingErdos(iterations_per_size, range_of_sizes, q, query):
    for size in range_of_sizes:
        LocalSearch_Scalability_Erdos(iterations_per_size, q, query, size, 'Steiner_time_test_n'+repr(size)+'_yes.txt')
    

def ScalingExperiment(G, q, querysize, d_weights, iterations, fileNo, fileYes):
    alpha = log((1-q)/q,2)
    beta = len(G.nodes())*log(1/(1-q),2)
    teller = 0
    while teller<iterations:
        query = list(GenerateQuerySet(G, random.choice(G.nodes()), querysize, 1, 1))
        if len(query)==querysize:
            teller+=1
            print(query)
            t0 = time()
            cycle = Heuristic_Steiner(G, 1, q, query, d_weights, 10)[0]
            t1 = time()
            if cycle == 'No cycle of maxlength exists between the query nodes':
                with open(fileNo, 'a') as file:
                    file.write(repr(round(t1-t0,5))+'\n') 
            else:
                with open(fileYes, 'a') as file:
                    file.write(repr(round(t1-t0,5))+'\n')
                    
            
def GenerateQuerySet(G, node_initial, k, beamwidth, s):
    initNeighbors = G.neighbors(node_initial)
    cands = set(random.sample(initNeighbors, min(len(initNeighbors),beamwidth)))
    neighborsPicked = set()
    neighborsPicked.add(node_initial)
    query = set()
    query.add(node_initial)
    while len(query) < k and len(cands)>0:
        queryaddtest = set()
        candsNow = cands.copy()
        for i in candsNow:
            if random.uniform(0,1) < s:
                queryaddtest.add(i)
                cands.remove(i)
            if i not in neighborsPicked:
                neighborsPicked.add(i)
                iNeighbors = G.neighbors(i)
                cands.update(set(random.sample(iNeighbors, min(len(iNeighbors),beamwidth))))
        query.update(set(random.sample(queryaddtest,min(k-len(query),len(queryaddtest)))))
    return query

        
        
