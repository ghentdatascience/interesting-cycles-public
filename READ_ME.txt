THIS CODE IS MADE AVAILABLE FOR REVIEWING PURPOSES ONLY

How to use MSIC and k-MSIC algorithms:
---------------------------------------

First import all functions:

from CycleAlgos import * 

- G denotes a graph in the python NetworkX package

To load a dataset as NetworkX graph G, simply run either foodcycle.py, GnutellaNetwork.py, EnronNetwork.py, TradeNetwwork.py or EnronNetwork.py

- d_weights is dict., keys are edges, values are edgeweights.
- q is the parameter from the objective functions
- query is a set of query nodes
- number_of_tries is the times the k-MSIC algo is repeated
- maxlength is the max. length of a returned cycle

MSIC:


MaxMeanCycle_Karp(G, d_weights)

k-MSIC:

Heuristic_Steiner(G, number_of_tries, q, query, d_weights, maxlength)

To reproduce the experiments in the CIKM paper:

compareKarpVsForsied(iterations, q, size_graph) was used to generate Fig. 5
LocalSearch_Performance_Erdos(iterations, q, query, size_graph) was used to generate Fig. 6
ScalingExperiment(G, q, querysize, d_weights, iterations, fileNo, fileYes) was used to run timing experiments from Fig. 7



To fit background MaxEnt models with prior beliefs:
---------------------------------------------------

maxent_fitting_geometric.m : use when there is a prior on weighted in-and out degrees

Newton_Geometric_Blocks.m : use when there is a combined prior on weighted in-and out degrees + block densitities

Fitting_Trade.m and Fitting_Trade_PriorTradeAg.m are used for fitting these models on the Trade Dataset.
See https://link.springer.com/article/10.1007/s10618-019-00627-1 for more information on these models + public repo.