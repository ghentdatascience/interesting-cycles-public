from CycleAlgos import *

with open('Email-Enron.txt', 'r') as file:
    # Creating the graph, and storing the explicit trade volumes as weights
    G = nx.DiGraph()
    for line in file:
        vals = line.split()
        G.add_edge(int(vals[0]),int(vals[1]))

d_weights = {}
for e in G.edges():
    d_weights[e] = 1
    
