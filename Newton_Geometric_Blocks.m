function [lar, lac, lalpha, ldiag, jr, jc, ev, cum] = Newton_Geometric_Blocks(D, nit, ind)
% Newton's method on the Lagrange func. with constraints on certain blocks
% (f.e. Trade Agreements)
% ind indicates the bin an element is part of.
% IMPORTANT: unlike DAG, NO diagonal shaped bins, and NO prior on last bin
% An extra constraint on the diagonal elements is implemented

drows = full(sum(D,2));
dcols = full(sum(D,1))';

bins = cell(1, max(ind)); % Creating empty cells. 
nu_r = cell(1, max(ind));
nu_c = cell(1, max(ind));
j_r = cell(1, max(ind));
j_c = cell(1, max(ind));

l_r = zeros(1, max(ind));
l_c = zeros(1, max(ind));
occur_r = cell(1, max(ind));
occur_c = cell(1, max(ind));

for i=1:max(ind)
    bins{i} = find(ind==i); % Every cell element is a vector with the resp. bin nodes.
    [nu_r{i}, ~, j_r{i}] = unique(drows(bins{i})); % Finding the unique row degrees in the ith bin.
    [nu_c{i}, ~, j_c{i}] = unique(dcols(bins{i})); % Finding the unique column degrees in the ith bin.
    
    l_r(i) = length(nu_r{i}); % Finding the number of occurences of elements in nu_r in the ith bin.
    occur_r{i} = zeros(l_r(i),1);
    for j=1:l_r(i)
        occur_r{i}(j)=length(find(j_r{i}==j));  
    end
    
    l_c(i) = length(nu_c{i}); % Finding the number of occurences of elements in nu_c in the ith bin.
    occur_c{i} = zeros(l_c(i),1);
    for j=1:l_c(i)
        occur_c{i}(j)=length(find(j_c{i}==j));
    end   
end

% Computing the actual Bin sums.
ActualBinSums = zeros(1,max(ind)-1); % The last bin is not counted in the prior.
for i=1:max(ind)-1
    ActualBinSums(i) =  sum(sum(D(ind==i,ind==i)));
end

% Computing the actual Diagonal sum
ActualDiagonalSum = sum(diag(D));

% Initializing parameters.
sumlr = sum(l_r);
csumlr = cumsum(l_r);
sumlc = sum(l_c);
csumlc = cumsum(l_c);
numberofbins = length(bins);

% Computing the lin. indices of the selfloops in the red. matrix.
% rowD = cat(1, j_r{:});
% temp = [0 csumlr(1:end-1)];
% rowD = rowD + temp(ind)';

unr = cat(1, nu_r{:});
[~,rowD] = ismember(drows,unr);

% colD = cat(1, j_c{:});
% temp = [0 csumlc(1:end-1)];
% colD = colD + temp(ind)';

unc = cat(1, nu_c{:});
[~,colD] = ismember(dcols,unc);

linIndices = sub2ind([sumlr sumlc], rowD, colD);

MatrixOccurR = repmat(occur_r{1}, 1, sumlc); % Defining the Occurence matrices.
MatrixOccurC = repmat(occur_c{1}', sumlr, 1);
for p=2:numberofbins
    MatrixOccurR = [MatrixOccurR; repmat(occur_r{p}, 1, sumlc)];
    MatrixOccurC = [MatrixOccurC repmat(occur_c{p}', sumlr, 1)];
end

MatrixOccurD = zeros(sumlr, sumlc);
val = unique(linIndices);
MatrixOccurD(val) = histc(linIndices,val);

x = -ones(1, sumlr+sumlc+numberofbins-1+1); % Number of priors = one less than number of bins + diag. constraint as last variable

[Grad, H] = GH_Geometric_Blocks(x, linIndices, l_r, l_c, occur_r, occur_c, nu_r, nu_c, ActualBinSums, ActualDiagonalSum);
epsiH=10^(-8);

alpha=logspace(0,-10, 50);
for k=1:nit
    % Adding a diagonal epsi term to H to make H pos. def.
    delta = -(H+epsiH*eye(sumlr+sumlc+numberofbins-1+1))\Grad;
    
    % Armijo Rule.
    % disp(norm(Grad))
    for i=1:length(alpha)
        
        % Checking the valid region for the geo. distribution
        xtest = x+alpha(i)*delta';
        
        Ar = repmat(xtest(1:sumlr)', 1, sumlc);
        Ac = repmat(xtest(sumlr+1:sumlr+sumlc), sumlr, 1);
        
        MatrixAlpha = zeros(sumlr, sumlc);
        startr = 1;
        startc = 1;
        for j=1:numberofbins-1
            MatrixAlpha(startr:csumlr(j), startc:csumlc(j)) = ones(csumlr(j)-startr+1, csumlc(j)-startc+1)*xtest(sumlr+sumlc+j);
            startr = csumlr(j)+1;
            startc = csumlc(j)+1;
        end
        
        MatrixDiag = zeros(sumlr, sumlc);
        MatrixDiag(linIndices) = xtest(sumlr+sumlc+numberofbins-1+1);
        reg1 = ((MatrixOccurR.*MatrixOccurC-MatrixOccurD)>0).*(Ar+Ac+MatrixAlpha);
        reg2 = (MatrixOccurD>0).*(Ar+Ac+MatrixAlpha+MatrixDiag);
        if max(max(reg1))<=0 && max(max(reg2))<=0 && obj_Geometric_Blocks(xtest, linIndices, l_r, l_c, occur_r, occur_c, nu_r, nu_c, ActualBinSums, ActualDiagonalSum) <= obj_Geometric_Blocks(x, linIndices, l_r, l_c, occur_r, occur_c, nu_r, nu_c, ActualBinSums, ActualDiagonalSum) + 10^(-1)*alpha(i)*dot(delta,Grad)
            x = xtest;
            break
        end
    end
    [Grad, H] = GH_Geometric_Blocks(x, linIndices, l_r, l_c, occur_r, occur_c, nu_r, nu_c, ActualBinSums, ActualDiagonalSum);
end
disp(norm(Grad))
lar = x(1:sumlr);
lac = x(sumlr+1:sumlr+sumlc);
lalpha = x(sumlr+sumlc+1:sumlr+sumlc+numberofbins-1);
ldiag = x(sumlr+sumlc+numberofbins-1+1);
jr = rowD;
jc  = colD;

lar2 = lar(jr);
lac2 = lac(jc);
RowM = repmat(lar2(:), 1, length(D));
ColumnM = repmat(lac2(:)', length(D), 1);
BinM = zeros(size(D));
for i=1:numberofbins-1
    BinM(bins{i},bins{i})= lalpha(i)*ones(length(bins{i}),length(bins{i}));
end
DiagM = diag(ldiag*ones(length(D),1));

ev = exp(RowM+ColumnM+BinM+DiagM)./(1-exp(RowM+ColumnM+BinM+DiagM)); % Expected value ~ geo. distr.

cum = exp(RowM+ColumnM+BinM+DiagM).^D;

%figure,imagesc(cum),colorbar